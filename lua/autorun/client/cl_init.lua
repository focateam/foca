include("foca/shared/sh_foca.lua")
include("foca/shared/sh_user.lua")

include("foca/client/cl_foca.lua")
include("foca/client/cl_spritesheet.lua")
include("foca/client/cl_fonts.lua")
include("foca/client/cl_scoreboard_panels.lua")
include("foca/client/cl_scoreboard.lua")