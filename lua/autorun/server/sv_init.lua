resource.AddSingleFile("resource/fonts/Montserrat-Regular.ttf")
resource.AddSingleFile("resource/fonts/Montserrat-Bold.ttf")
resource.AddFile("materials/scoreboard/sprites.png")

AddCSLuaFile("autorun/client/cl_init.lua")

AddCSLuaFile("foca/shared/sh_foca.lua")
AddCSLuaFile("foca/shared/sh_user.lua")
AddCSLuaFile("foca/client/cl_foca.lua")
AddCSLuaFile("foca/client/cl_spritesheet.lua")
AddCSLuaFile("foca/client/cl_fonts.lua")
AddCSLuaFile("foca/client/cl_scoreboard_panels.lua")
AddCSLuaFile("foca/client/cl_scoreboard.lua")

-- Installation script, sets up database etc.
include("foca/server/sv_setup.lua")

include("foca/shared/sh_foca.lua")
include("foca/shared/sh_user.lua")

include("foca/server/sv_foca.lua")
include("foca/server/sv_user.lua")
include("foca/server/sv_scoreboard.lua")
