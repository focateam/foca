foca.scoreboard = {}

local COLOR = foca.COLOR

local scoreboardContainer
local userPanels = {}
local headerRibbons = {}
local callbacks = {}

local numPlayers = 0

local function CreateContainer()
	local width = 820
	local height = 900

	local container = vgui.Create("DPanel")
	container:SetSize(width, height)
	container:SetPos((surface.ScreenWidth() - width) / 2, 100)
	container:SetDrawBackground(false)
	container:SetVisible(false)

	return container
end

local function CreateHeader(parent)
	local header = vgui.Create("FocaSBPanel", parent)
	header:SetSize(820, 60)
	header:SetPos(0, 0)
	header:SetBackgroundColor(COLOR.header)

	local title = vgui.Create("DLabel", header)
	title:SetFont("Foca_SBHeader")
	title:SetText(GetHostName())
	title:SetTextColor(Color(255, 255, 255))
	title:SizeToContents()
	title:SetPos(22, 0)
	title:CenterVertical(0.5)

	local playerCount = vgui.Create("DLabel", header)
	playerCount:SetFont("Foca_SBHeader")
	playerCount:SetTextColor(Color(255, 255, 255))
	playerCount:CenterVertical(0.5)
	callbacks.UpdatePlayerCount = function()
		playerCount:SetText(table.Count(player.GetAll()) .. "/" .. game.MaxPlayers())
		playerCount:SizeToContents()
		playerCount:SetPos(header:GetWide() - playerCount:GetWide() - 22, 0)
		playerCount:CenterVertical(0.5)
	end
	callbacks.UpdatePlayerCount()

	return header
end

local function CreateUserPanel(ply)
	local panel = vgui.Create("FocaSBUserPanel", scoreboardContainer)
	panel:SetPlayer(ply)
	userPanels[ply] = panel
    numPlayers = numPlayers + 1
	panel:SetIndex(numPlayers)
	callbacks.UpdatePlayerCount()

	return panel
end

local function PlaceRibbons(header)
	local scX, scY = scoreboardContainer:GetPos()
	local headerX, headerY = header:GetPos()
	headerX = headerX + scX
	headerY = headerY + scY
	local headerW, headerH = header:GetSize()
	local headerBottom = headerY + headerH
	local headerRight = headerX + headerW

	headerRibbons.right = {
		{ x = headerRight - 10, y = headerBottom },
		{ x = headerRight, y = headerBottom },
		{ x = headerRight - 10, y = headerBottom + 6 }
	}
	headerRibbons.left = {
		{ x = headerX, y = headerBottom },
		{ x = headerX + 10, y = headerBottom },
		{ x = headerX + 10, y = headerBottom + 6 }
	}
end

local function RefreshList()
    local index = 1
	for _, panel in pairs(userPanels) do
		panel:SetIndex(index)
        index = index + 1
	end

	foca.scoreboard.UpdatePanelPositions()
end

function foca.scoreboard.UpdatePanelPositions()
	local previous
	for _, panel in pairs(userPanels) do
		local parentWidth, parentHeight = panel:GetParent():GetSize()
		local posY = 60
		if previous then
			local previousX, previousY = previous:GetPos()
			posY = previousY + previous:GetTall()
		end
		panel:SetPos((parentWidth - 800) / 2, posY)

		previous = panel
	end
end

function foca.scoreboard.Setup()
	if scoreboardContainer then
		scoreboardContainer:Remove()
	end

	scoreboardContainer = CreateContainer()
	local header = CreateHeader(scoreboardContainer)
	PlaceRibbons(header)

	foca.scoreboard.UpdateUserPanels()
end

function foca.scoreboard.UpdateUserPanels()
    for _, ply in pairs(player.GetAll()) do
        foca.scoreboard.UpdateUserPanel(ply)
    end
end

--TODO Refactor with userdata instead of player when user has been implemented
function foca.scoreboard.UpdateUserPanel(ply)
	if not scoreboardContainer or not IsValid(scoreboardContainer) then
		--foca.scoreboard.Setup()
		return
	end

	local userPanel = userPanels[ply]
	if not userPanel then
		userPanel = CreateUserPanel(ply)
		local userPanel2 = CreateUserPanel({
			Ping = function(self) return 42 end,
			GetUserGroup = function(self) return "user" end,
			GetName = function(self) return "Mock" end
        })
		userPanel2.info:UpdatePing()
    end

    userPanel.info:UpdatePing()
    RefreshList()
	--TODO Add updating of userdata
end

function foca.scoreboard.RemoveUserPanel(ply)
	userPanels[ply] = nil
    numPlayers = numPlayers - 1

	RefreshList()
	callbacks.UpdatePlayerCount()
end

hook.Add("ScoreboardShow", "foca_scoreboard_show", function()
	if not scoreboardContainer or not IsValid(scoreboardContainer) then
		foca.scoreboard.Setup()
	end

	gui.EnableScreenClicker(true)
	scoreboardContainer:SetVisible(true)
	return false
end)

hook.Add("ScoreboardHide", "foca_scoreboard_hide", function()
	gui.EnableScreenClicker(false)
	scoreboardContainer:SetVisible(false)
	return false
end)

hook.Add("HUDDrawScoreBoard", "foca_scoreboard_draw", function()
	if scoreboardContainer and IsValid(scoreboardContainer) and scoreboardContainer:IsVisible() then
		surface.SetDrawColor(COLOR.header_shadow)
		surface.DrawPoly(headerRibbons.right)
		surface.DrawPoly(headerRibbons.left)
	end

	return false
end)

--TODO Move this somewhere more appropriate
net.Receive("foca_userdata", function()
	local steamID = net.ReadString()
	local hookID = "foca_scoreboard_playerinit_" .. steamID
	hook.Add("Think", hookID, function()
		for _, ply in pairs(player.GetAll()) do
			if ply:SteamID() == steamID and ply:GetName() ~= "unconnected" then
				foca.scoreboard.UpdateUserPanel(ply)
				hook.Remove("Think", hookID)
                break
			end
		end
	end)
end)

timer.Create("Foca_SBUpdate", 3, 0, foca.scoreboard.UpdateUserPanels)