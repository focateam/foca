local fontName = "Montserrat-Regular"

local sb_header = {
	font = fontName,
	size = 45
}
local sb_name = {
	font = fontName,
	size = 30
}
local sb_rank = {
	font = fontName,
	size = 17
}
local sb_ping = {
	font = fontName,
	size = 18
}
local sb_button = {
	font = fontName,
	size = 16
}

surface.CreateFont("Foca_SBHeader", sb_header)
surface.CreateFont("Foca_SBName", sb_name)
surface.CreateFont("Foca_SBRank", sb_rank)
surface.CreateFont("Foca_SBPing", sb_ping)
surface.CreateFont("Foca_SBButton", sb_button)