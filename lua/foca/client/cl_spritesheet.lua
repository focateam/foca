SKIN = {}

SKIN.PrintName = "Foca"
SKIN.Author = "The Foca Team"
SKIN.DermaVersion = 1
SKIN.GwenTexture = Material("scoreboard/sprites.png")

SKIN.tex = {}

SKIN.tex.Avatar = {}
SKIN.tex.Avatar.Primary = GWEN.CreateTextureNormal(0, 0, 40, 40)
SKIN.tex.Avatar.Secondary = GWEN.CreateTextureNormal(40, 0, 40, 40)

SKIN.tex.Button = {}

SKIN.tex.Button.Cleanup = {}
SKIN.tex.Button.Cleanup.Click = GWEN.CreateTextureNormal(0, 40, 16, 16)
SKIN.tex.Button.Cleanup.Hover = GWEN.CreateTextureNormal(0, 56, 16, 16)
SKIN.tex.Button.Cleanup.Normal = GWEN.CreateTextureNormal(0, 72, 16, 16)

SKIN.tex.Button.Kick = {}
SKIN.tex.Button.Kick.Click = GWEN.CreateTextureNormal(16, 40, 16, 16)
SKIN.tex.Button.Kick.Hover = GWEN.CreateTextureNormal(16, 56, 16, 16)
SKIN.tex.Button.Kick.Normal = GWEN.CreateTextureNormal(16, 72, 16, 16)

SKIN.tex.Button.Mute = {}
SKIN.tex.Button.Mute.Click = GWEN.CreateTextureNormal(32, 40, 16, 16)
SKIN.tex.Button.Mute.Hover = GWEN.CreateTextureNormal(32, 56, 16, 16)
SKIN.tex.Button.Mute.Normal = GWEN.CreateTextureNormal(32, 72, 16, 16)

SKIN.tex.Button.Ban = {}
SKIN.tex.Button.Ban.Click = GWEN.CreateTextureNormal(48, 40, 16, 16)
SKIN.tex.Button.Ban.Hover = GWEN.CreateTextureNormal(48, 56, 16, 16)
SKIN.tex.Button.Ban.Normal = GWEN.CreateTextureNormal(48, 72, 16, 16)

SKIN.tex.Button.Report = {}
SKIN.tex.Button.Report.Click = GWEN.CreateTextureNormal(64, 40, 16, 16)
SKIN.tex.Button.Report.Hover = GWEN.CreateTextureNormal(64, 56, 16, 16)
SKIN.tex.Button.Report.Normal = GWEN.CreateTextureNormal(64, 72, 16, 16)

SKIN.tex.Button.Unmute = {}
SKIN.tex.Button.Unmute.Click = GWEN.CreateTextureNormal(80, 40, 16, 16)
SKIN.tex.Button.Unmute.Hover = GWEN.CreateTextureNormal(80, 56, 16, 16)
SKIN.tex.Button.Unmute.Normal = GWEN.CreateTextureNormal(80, 72, 16, 16)

SKIN.tex.Signal = {}
SKIN.tex.Signal.Shit = GWEN.CreateTextureNormal(0, 88, 36, 25)
SKIN.tex.Signal.Good = GWEN.CreateTextureNormal(36, 88, 36, 25)
SKIN.tex.Signal.Awesome = GWEN.CreateTextureNormal(72, 88, 36, 25)

SKIN.tex.PanelShadow = GWEN.CreateTextureNormal(0, 113, 108, 15)

derma.DefineSkin("Foca", "SKIN for Foca", SKIN)