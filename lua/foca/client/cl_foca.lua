foca.COLOR = {
	header = Color(68, 160, 222),
	header_shadow = Color(55, 131, 183),
	row_primary = Color(255, 255, 255),
	row_secondary = Color(239, 239, 239),
	player_name = Color(51, 51, 51),
	player_rank = Color(117, 117, 117),
	player_actions_bg = Color(223, 223, 223),
	button = Color(170, 170, 170),
	button_hover = Color(102, 102, 102),
	button_click = Color(51, 51, 51),
	ping_text = Color(51, 51, 51)
}