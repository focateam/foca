local COLOR = foca.COLOR

local PANEL = {}
function PANEL:Paint()
	surface.SetDrawColor(self:GetBackgroundColor())
	surface.DrawRect(0, 0, self:GetWide(), self:GetTall())
end
vgui.Register("FocaSBPanel", PANEL, "DPanel")

PANEL = {
	ply = nil,
	avatar = nil,

	labels = {
		name = nil,
		rank = nil,
		ping = nil
	}
}
function PANEL:Init()
	self:SetSize(800, 64)

	local actionsButton = vgui.Create("FocaSBInvisibleButton", self)
	actionsButton:SetSize(self:GetSize())
	actionsButton:SetPos(0, 0)
	actionsButton.DoClick = function()
		self:GetParent():ToggleShowActions()
	end

	self.avatar = vgui.Create("FocaSBAvatar", self)

	self.labels.name = vgui.Create("DLabel", self)
	local name = self.labels.name
	name:SetFont("Foca_SBName")
	name:SetTextColor(COLOR.player_name)
	name:SetPos(58, 0)
	name:CenterVertical(0.27)

	self.labels.rank = vgui.Create("DLabel", self)
	local rank = self.labels.rank
	rank:SetFont("Foca_SBRank")
	rank:SetText("")
	rank:SetTextColor(COLOR.player_rank)
	rank:SetPos(58, 0)
	rank:CenterVertical(0.74)

	self.ping = vgui.Create("FocaSBPing", self)
	self.ping:CenterVertical()
	self.ping:AlignRight()
end
function PANEL:SetPlayer(ply)
	self.ply = ply
	self.avatar:SetPlayer(ply)
	self.labels.name:SetText(ply:GetName())
	self.labels.name:SizeToContents()
	self.labels.rank:SetText(foca.ranks[ply:GetUserGroup()].Title)
	self.labels.rank:SizeToContents()
end
function PANEL:GetPlayer()
	return self.ply
end
function PANEL:UpdatePing()
	self.ping:Update(self.ply:Ping())
end
vgui.Register("FocaSBUserInfo", PANEL, "FocaSBPanel")

PANEL = {}
function PANEL:Init()
	self:SetSize(800, 30)
	self:SetBackgroundColor(COLOR.player_actions_bg)

	local buttons = derma.GetNamedSkin("Foca").tex.Button
	local kickButton = vgui.Create("FocaSBImageButton", self)
	kickButton:SetTexture(buttons.Kick)
	kickButton:SetSize(16, 16)
	kickButton:SetPos(5, 5)
	function kickButton.DoClick()
		LocalPlayer():ConCommand("kick " .. self:GetParent():GetPlayer():Name())
	end
	local cleanButton = vgui.Create("FocaSBImageButton", self)
	cleanButton:SetTexture(buttons.Cleanup)
	cleanButton:SetSize(16, 16)
	cleanButton:SetPos(30, 5)
	function cleanButton.DoClick()
        net.Start("foca_useraction")
		net.WriteString("cleanup")
		net.WriteEntity(self:GetParent():GetPlayer())
        net.SendToServer()
	end
end
function PANEL:Paint()
	derma.GetNamedSkin("Foca").tex.PanelShadow(0, 0, self:GetSize())
end
vgui.Register("FocaSBUserActions", PANEL, "FocaSBPanel")

PANEL = {
	ply = nil,
	showActions = false
}
function PANEL:Init()
	self.info = vgui.Create("FocaSBUserInfo", self)
	self.actions = vgui.Create("FocaSBUserActions", self)
	self.actions:SetPos(0, self.info:GetTall())

	self:SetShowActions(false)
end
function PANEL:SetIndex(index)
	local parent = self:GetParent()
	if not parent or not IsValid(parent) then
		return
	end

	local sequence = index % 2
	local color = COLOR.row_primary
	if (sequence == 0) then
		color = COLOR.row_secondary
	end

	self.info:SetBackgroundColor(color)
	self.info.avatar:SetSequence(sequence)
end
function PANEL:GetPlayer()
	return self.ply
end
function PANEL:SetPlayer(ply)
	self.ply = ply
	self.info:SetPlayer(ply)
end
function PANEL:SetShowActions(show)
	self.showActions = show
	local newHeight = self.info:GetTall()
	if show then
		newHeight = newHeight + self.actions:GetTall()
	end
	self:SetSize(self.info:GetWide(), newHeight)

	foca.scoreboard.UpdatePanelPositions()
end
function PANEL:ToggleShowActions()
	self:SetShowActions(not self.showActions)
end
vgui.Register("FocaSBUserPanel", PANEL, "DPanel")

PANEL = {
	avatarMask = nil,
	avatarButton = nil
}
function PANEL:Init()
	self:SetSize(40, 40)
	self:SetPos(10, 0)
	self:CenterVertical(0.5)

	self.avatarMask = vgui.Create("FocaSBAvatarMask", self)
	self.avatarMask:SetSize(self:GetSize())
	self.avatarMask:SetPos(0, 0)

	self.avatarButton = vgui.Create("FocaSBInvisibleButton", self)
	self.avatarButton:SetSize(self:GetSize())
	self.avatarButton:Center()
end
function PANEL:SetPlayer(ply)
	getmetatable(self).SetPlayer(self, ply)
	self.avatarButton.DoClick = function()
		ply:ShowProfile()
	end
end
function PANEL:SetSequence(sequence)
	self.avatarMask:SetSequence(sequence)
end
vgui.Register("FocaSBAvatar", PANEL, "AvatarImage")

PANEL = {}
function PANEL:Init()
	self:SetText("")
end
function PANEL:Paint()
	return
end
vgui.Register("FocaSBInvisibleButton", PANEL, "DButton")

PANEL = {
	texture = nil
}
function PANEL:Init()
	self:SetText("")
end
function PANEL:SetTexture(texture)
	self.texture = texture
end
function PANEL:Paint()
	--getmetatable(self).Paint(self)
	if self:IsDown() then
		self.texture.Click(0, 0, self:GetSize())
	elseif self:IsHovered() then
		self.texture.Hover(0, 0, self:GetSize())
	else
		self.texture.Normal(0, 0, self:GetSize())
	end
end
vgui.Register("FocaSBImageButton", PANEL, "FocaSBInvisibleButton")

PANEL = {sequence = 0}
function PANEL:SetSequence(sequence)
	self.sequence = sequence
end
function PANEL:Paint()
	local avatar = derma.GetNamedSkin("Foca").tex.Avatar
	local x, y = self:GetPos()
	if self.sequence == 1 then
		avatar.Primary(x, y, self:GetSize())
	elseif self.sequence == 0 then
		avatar.Secondary(x, y, self:GetSize())
	end
end
vgui.Register("FocaSBAvatarMask", PANEL, "Panel")

PANEL = {}
function PANEL:Init()
	self:SetSize(50, 50)

	self.signal = vgui.Create("FocaSBPingSignal", self)
	self.signal:CenterVertical(0.25)

	self.latency = vgui.Create("DLabel", self)
	self.latency:SetFont("Foca_SBPing")
	self.latency:SetTextColor(COLOR.ping_text)
	self.latency:SizeToContentsY()
	self.latency:CenterVertical(0.75)
end
function PANEL:Update(ping)
	self.latency:SetText(ping .. "ms")
	self.latency:SizeToContentsX()
	self.latency:AlignLeft()
	self.signal:Update(ping)
    self.signal:AlignLeft()
end
vgui.Register("FocaSBPing", PANEL, "Panel")

PANEL = {ping = 0}
function PANEL:Init()
	self:SetSize(36, 25)
end
function PANEL:Update(ping)
	self.ping = ping
end
function PANEL:Paint()
	local signal = derma.GetNamedSkin("Foca").tex.Signal
	local x, y = self:GetPos()
	if self.ping <= 75 then
		signal.Awesome(x, y, self:GetSize())
	elseif self.ping <= 150 then
		signal.Good(x, y, self:GetSize())
	else
		signal.Shit(x, y, self:GetSize())
	end
end
vgui.Register("FocaSBPingSignal", PANEL, "Panel")