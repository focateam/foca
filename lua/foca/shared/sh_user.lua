-- Default ranks
foca.ranks.user = {}
foca.ranks.user.Title = "Player"

foca.ranks.moderator = {}
foca.ranks.moderator.Title = "Moderator"

foca.ranks.superadmin = {}
foca.ranks.superadmin.Title = "Administrator"