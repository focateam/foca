foca = {}
foca.ranks = {}

foca.log = {}
foca.log.DEBUG = 3
foca.log.INFO = 2
foca.log.WARNING = 1
foca.log.ERROR = 0

foca.log.verbosity = foca.log.DEBUG

function foca.log.println(msg)
    print("[FOCA] " .. msg .. "\n")
end

function foca.log.debug(msg)
    if foca.log.verbosity >= foca.log.DEBUG then foca.log.println("DEBUG: " .. msg) end
end

function foca.log.info(msg)
    if foca.log.verbosity >= foca.log.INFO then foca.log.println(msg) end
end

function foca.log.warning(msg)
    if foca.log.verbosity >= foca.log.WARNING then foca.log.println("WARNING: " .. msg) end
end

function foca.log.error(msg)
    if foca.log.verbosity >= foca.log.ERROR then foca.log.println("ERROR: " .. msg) end
end

foca.upSince = SysTime()