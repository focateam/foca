function foca.SetRank(ply, rank)
    ply:SetUserGroup(rank)
	sql.Query("INSERT OR IGNORE INTO foca_users (id, rank) VALUES(" .. ply:UniqueID() .. ", " .. sql.SQLStr(rank) .. ")")
	sql.Query("UPDATE foca_users SET rank = " .. sql.SQLStr(rank) .. " WHERE id = " .. ply:UniqueID())

	print("Usergroup of " .. ply:GetName() .. " is " .. ply:GetUserGroup())--TODO This is debug
end

-- Server only command to give administrative privileges
concommand.Add("foca_setowner", function(ply, cmd, args, str)
		local userID = tonumber(args[1])
        -- TODO Syntax check
		for _, ply in pairs(player.GetHumans()) do
			if ply:UserID() == userID then
				foca.SetRank(ply, "superadmin")
				break
			end
		end
    end
)

-- Get the player's rank if it's stored in the database
hook.Add("PlayerInitialSpawn", "foca_allranks", function(ply)
        -- TODO Add error handling on SQL queries
        local plyRank = sql.QueryValue("SELECT rank FROM foca_users WHERE id = " .. ply:UniqueID())
        if plyRank ~= nil then
            ply:SetUserGroup(plyRank)
		end

		print("Usergroup of " .. ply:GetName() .. " is " .. ply:GetUserGroup())--TODO This is debug
    end
)