--TODO Move this somewhere more appropriate

util.AddNetworkString("foca_userdata")
util.AddNetworkString("foca_useraction")

function foca.SendUserData(ply, client)
	net.Start("foca_userdata")
	net.WriteString(ply:SteamID())
	--expand this with more data as needed
	if client then
		net.Send(client)
		print("Sent to:")
		print(client)
	else
		net.Send(player.GetAll())
	end
end

hook.Add("PlayerInitialSpawn", "foca_scoreboard_playerinitspawn", function(ply)
	foca.SendUserData(ply)
end)

--TODO work with usergroups
--TODO abstraction and stuff
net.Receive("foca_useraction", function(len, ply)
	local action = net.ReadString()
	local targetPlayer = net.ReadEntity()
	if (action == "cleanup") then
		if (ply:IsAdmin() or ply:IsSuperAdmin()) then
			local propList = cleanup.GetList()[targetPlayer:UniqueID()]
			local props = propList["props"]
			for _, ent in ipairs(props) do
				ent:Remove()
			end
			propList["props"] = {};
		end
	end
end)